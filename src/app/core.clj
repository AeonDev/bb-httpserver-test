(ns app.core
  (:require [org.httpkit.server :as http]
            [ruuter.core :as ruuter]
            [hiccup.core :as h]))

(def routes [{:path "/"
              :method :get
              :response {:status 200
                         :body (h/html 
                                 [:p "Hi BB!"])}}])

(defn -main []
  (http/run-server #(ruuter/route routes %) {:port 8080})
  (println "http://localhost:8080")  
  @(promise))
